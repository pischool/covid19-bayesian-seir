Please, read the original [paper](https://github.com/jriou/covid_adjusted_cfr/blob/master/manuscript/manuscript_v2.pdf) and [code](https://github.com/jriou/covid_adjusted_cfr)

# How to use R experiment

1. Install R and Rstudio (example using Linux-Mint-19)

```bash
sudo apt update
sudo apt-get install r-base
sudo apt-get install r-base-dev xml2 libxml2-dev libssl-dev libcurl4-openssl-dev unixodbc-dev
sudo apt-get install wget
wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.1578-amd64.deb
sudo dpkg -i rstudio-1.2.1578-amd64.deb
```

Note: update R: https://www.r-bloggers.com/updating-r-on-ubuntu/


2. Install some libraries in the rstudio console

```bash
install.packages("dplyr")
install.packages("rstan")
install.packages("tidyr")
install.packages("cowplot")
install.packages(c("lubridate", "readxl", "rstan", "tidyverse"))
install.packages("xtable")
install.packages("devtools")
packageurl <- "http://cran.r-project.org/src/contrib/Archive/rstan/rstan_2.19.2.tar.gz"
install.packages(packageurl, repos=NULL, type="source")
```


3. Download project

```bash
git clone https://github.com/jriou/covid_adjusted_cfr.git
```


4. In Rstudio, set the folder `covid19-bayesian-seir` as working directory (the R project should be in the `r-experiment` folder)


5. In Rstudio, run some model

```bash
source('r-experiment/run_models/run_model13_china_small.R')
```

6. In Rstudio, run some figure outputs

```bash
source("r-experiment/format_output/model13_italy/output_model13_italy.R")
```
