import random

from pyexperiment.dataset.italy import ItalyDataset
from pyexperiment.model.italy import ItalyModel

random.seed(101)


dataset = ItalyDataset()
dataset.show_info()
model = ItalyModel(dataset)
model.run()
model.show_info()
model.save_results()
