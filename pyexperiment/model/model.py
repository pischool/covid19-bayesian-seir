import os
import pickle
from datetime import datetime
import math
import random

import numpy as np
import pandas as pd
from pystan import StanModel, check_hmc_diagnostics
from scipy import stats

class Model:

    T_md = None
    summary_dict = None

    def run(self):

        # Compute discrete distribution of time from onset to death
        # Fom Linton et al
        self.linton_mean = 20.2
        self.linton_sd = 11.6

        self.linton_pars = self.get_par_lnorm(self.linton_mean, self.linton_sd)
        print(self.linton_pars)

        # Discretize
        self.gamma = np.zeros(60)

        self.linton_pars['sigma']

        for i in range(1, len(self.gamma)+1):
            self.gamma[i-1] = (
                stats.lognorm.cdf(i+.5, s=self.linton_pars['sigma'], scale = math.exp(self.linton_pars['mu'])) -
                stats.lognorm.cdf(i-.5, s=self.linton_pars['sigma'], scale = math.exp(self.linton_pars['mu']))
            )

        self.gamma = self.gamma / sum(self.gamma)

        ## Format for stan
        t0 = 0
        S = datetime.strptime(self.dataset.day_max, "%Y-%m-%d") - datetime.strptime(self.dataset.day_start, "%Y-%m-%d")
        S = S.days
        t_data = datetime.strptime(self.dataset.day_data, "%Y-%m-%d") - datetime.strptime(self.dataset.day_start, "%Y-%m-%d")
        t_data = t_data.days
        ts = list(range(t_data, S+1))
        D = S - t_data + 1
        tswitch = datetime.strptime(self.dataset.day_quarantine, "%Y-%m-%d") - datetime.strptime(self.dataset.day_start, "%Y-%m-%d")
        tswitch = tswitch.days

        data_list_model13 = {
            'K': 9,
            'age_dist': self.dataset.age_dist,
            'pop_t': self.dataset.pop_t,
            't0': 0,
            't_data': t_data,
            'tswitch': tswitch,
            'S': S,
            'ts': ts,
            'D': len(self.dataset.incidence_cases),
            'incidence_cases': self.dataset.incidence_cases,
            'incidence_deaths': self.dataset.incidence_deaths,
            'agedistr_cases': self.dataset.cases_tmax,
            'agedistr_deaths': self.dataset.mort_tmax,
            'p_beta': 1,
            'p_eta': [1, 1],
            'p_pi': [1, 999],
            'p_incubation': 5.95,  # Bi et al
            'p_infectious': 2.4,  # Bi et al
            'p_epsilon': [1, 1],
            'p_phi': 1 / 100,
            'p_rho': [1, 1],
            'p_xi': 1,
            'p_nu': 1 / 5,
            'p_chi': 5,
            'G': 60,
            'p_gamma': self.gamma,
            'inference': 0,
            'doprint': 0,
            'contact': [
                5.13567073170732, 1.17274819632136, 0.982359525171638, 2.21715890088845, 1.29666356906914,
                0.828866413937242, 0.528700773224482, 0.232116187961884, 0.0975205061876398, 1.01399087153423,
                10.420788530466, 1.5084165224448, 1.46323525034693, 2.30050630727188, 1.0455742822567,
                0.396916593664865, 0.276112578159939, 0.0867321859134207, 0.787940961549209, 1.39931415327149,
                4.91448118586089, 2.39551550152373, 2.08291844616138, 1.67353143324194, 0.652483430981848,
                0.263165822550241, 0.107498717856296, 1.53454251726848, 1.17129688889679, 2.06708280469829,
                3.91165644171779, 2.74588910732349, 1.66499320847473, 1.02145416818956, 0.371633336270256,
                0.112670158106901, 0.857264438638371, 1.7590640625625, 1.71686658407219, 2.62294018855816,
                3.45916114790287, 1.87635185962704, 0.862205884832066, 0.523958801433231, 0.205791955532149,
                0.646645383952458, 0.943424739130445, 1.62776721065554, 1.87677409215498, 2.21415705015835,
                2.5920177383592, 1.10525460534109, 0.472961105423521, 0.282448363507455, 0.504954014454259,
                0.438441714821823, 0.77694120330432, 1.40954408148402, 1.24556204828388, 1.35307720400585,
                1.70385674931129, 0.812686154912104, 0.270111273681845, 0.305701280434649, 0.420580126969344,
                0.432113761275257, 0.707170907986224, 1.04376196943771, 0.798427737704416, 1.12065725135372,
                1.33035714285714, 0.322575366839763, 0.237578345845701, 0.24437789962337, 0.326505855457376,
                0.396586297530862, 0.758318763302674, 0.881999483055259, 0.688988121391528, 0.596692087603768,
                0.292682926829268
            ]
        }

        # TODO: refactor in a function in `utils.py` file
        # Avoiding recompilation: https://pystan.readthedocs.io/en/latest/avoiding_recompilation.html
        if not os.path.isfile(os.path.join('models', f'{self.name}.pickle')):
            md = StanModel(f'models/{self.stan_model_name}.stan', model_name='model13')
            with open(os.path.join('models', f'{self.name}.pickle'), 'wb') as f:
                pickle.dump(md, f, pickle.HIGHEST_PROTOCOL)

        else:
            f = open(os.path.join('models', f'{self.name}.pickle'), 'rb')
            md = pickle.load(f)
        # md = StanModel('models/model13d.stan', model_name='model13')

        data_list_model13['p_psi'] = [71,18]

        # CHECK: timelimit=2?, algorithm='NUTS'?
        self.T_md = md.sampling(data=data_list_model13, **self.params)

        check_hmc_diagnostics(self.T_md)
        self.summary_dict = self.T_md.summary()

    @staticmethod
    def get_par_lnorm(mn, sd):
        """Get lognormal parameters from mean and sd
        """
        mu = math.log(mn) - 1 / 2 * math.log((sd / mn) ** 2 + 1)
        sigma = math.sqrt(math.log((sd / mn) ** 2 + 1))
        # return {'mu': round(mu, 7), 'sigma': round(sigma, 7)}
        return {'mu': mu, 'sigma': sigma}

    def show_info(self):
        print(f"beta: {self.T_md['beta']}")
        # print(f"eta: {self.T_md['eta']}")
        print(f"epsilon: {self.T_md['epsilon']}")
        print(f"rho: {self.T_md['rho']}")
        print(f"pi: {self.T_md['pi']}")
        # print(f"xi: {self.T_md['xi']}")
        # print(f"nu: {self.T_md['nu']}")
        print(f"psi: {self.T_md['psi']}")

        print(f"cfr_A_symptomatic: {self.T_md['cfr_A_symptomatic']}")
        print(f"cfr_B_symptomatic: {self.T_md['cfr_B_symptomatic']}")
        print(f"cfr_C_symptomatic: {self.T_md['cfr_C_symptomatic']}")
        print(f"cfr_D_symptomatic: {self.T_md['cfr_D_symptomatic']}")
        print(f"cfr_C_all: {self.T_md['cfr_C_all']}")
        print(f"cfr_D_all: {self.T_md['cfr_D_all']}")

        print("cfr_A_symptomatic:  ", self.T_md["cfr_A_symptomatic"][-1],
              "\ncfr_B_symptomatic:  ", self.T_md["cfr_B_symptomatic"][-1],
              "\ncfr_C_symptomatic:  ", self.T_md["cfr_C_symptomatic"][-1],
              "\ncfr_D_symptomatic:  ", self.T_md["cfr_D_symptomatic"][-1],
              "\ncfr_C_all:          ", self.T_md["cfr_C_all"][-1],
              "\ncfr_D_all:          ", self.T_md["cfr_D_all"][-1])

        df = pd.DataFrame(
            self.summary_dict['summary'],
            columns=self.summary_dict['summary_colnames'],
            index=self.summary_dict['summary_rownames']
        )
        # df = pd.DataFrame(summary_dict['summary'], columns=summary_dict['summary_colnames'], index=summary_dict['summary_rownames'])
        df.to_csv(os.path.join('posterior', f'{self.name}_summary_dict_italy.pickle'))
        print(df)

    def save_results(self):
        if not os.path.isdir('posterior'):
            os.mkdir('posterior')

        param_dict = dict(self.T_md.extract())

        with open(os.path.join('posterior', f'{self.name}_posteriors_italy.pickle'), 'wb') as f:
            pickle.dump(param_dict, f)

        df = pd.DataFrame(
            self.summary_dict['summary'],
            columns=self.summary_dict['summary_colnames'],
            index=self.summary_dict['summary_rownames']
        )
        df.to_csv(os.path.join('posterior', f'{self.name}_summary_dict_italy.pickle'))
