from pyexperiment.model.model import Model


class ItalyCModel(Model):

    def __init__(self, dataset):
        self.dataset = dataset
        self.name = "model13_c_italy"
        self.stan_model_name = "model13c"
        self.params = {
            'iter': 10,
            'chains': 1,
            'thin': 1,
            'algorithm': 'NUTS',
            'warmup': 2,
            'verbose': True,
            'init': 0,
            'seed': 101,
            'control': {
                'max_treedepth': 10,
                'adapt_delta': 0.8,
                'adapt_init_buffer': 75,
                'adapt_term_buffer': 50,
                'adapt_window': 25
            }
        }
