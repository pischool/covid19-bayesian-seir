
class Dataset:

    def show_info(self):
        print(f"age_dist: {self.age_dist}")
        print(f"pop_t: {self.pop_t}")
        print(f"D: {len(self.incidence_cases)}")
        print(f"incidence_cases: {self.incidence_cases}")
        print(f"incidence_deaths: {self.incidence_deaths}")
        print(f"agedistr_cases: {self.cases_tmax}")
        print(f"agedistr_deaths: {self.mort_tmax}")

    def _get_age_structure_from_excel(self):
        df = pd.read_excel(os.path.join("data", "age_structure.xlsx"))
        df['country'] = df['country'].str.strip()
        df = df[df['country'] == 'Italy']
        df.drop(['country', 'all_classes'], axis=1, inplace=True)
        df = df.T
        df.rename(columns={df.columns[0]: "values"}, inplace=True)
        df['values'] = df['values'].astype(int)
        df['age'] = np.array([0,0,10,10,20,20,30,30,40,40,50,50,60,60,70,70,80,80,80,80,80])
        return df.groupby("age").sum()['values'].values
