import os
from datetime import timedelta, datetime

import numpy as np
import pandas as pd

from pyexperiment.dataset.dataset import Dataset


class ChinaDataset(Dataset):

    def __init__(self):
        self.data_dir = os.path.join("data", "china")

        # Controls ----
        self.day_start = "2019-12-31"
        self.day_data = "2020-01-01"
        self.day_max = "2020-02-11"
        self.day_quarantine = "2020-01-20"

        # Age distribution in China for 9 age classes
        # (https://www.worldometers.info/demographics/china-demographics/)
        self.age_dist = np.array([83932437 + 86735183,
                                  84262751 + 82341859,
                                  87158167 + 97989003,
                                  128738970 + 100091455,
                                  96274146 + 119837617,
                                  123445382 + 98740491,
                                  77514139 + 74149766,
                                  44949689 + 26544616,
                                  16181417 + 7581777 + 2305024 + 475193 + 74692])

        self.age_dist = self.age_dist / sum(self.age_dist)

        # Population in Hubei
        # (National Bureau of Statistics of China)
        self.pop_t = 59020000

        # Case incidence in Hubei up to 2020-02-11
        # (Chinese CDC Weekly, The epidemiological characteristics of an outbreak...)
        self.confirmed_cases = pd.read_csv(os.path.join(self.data_dir, "confirmed_cases.csv"))
        self.confirmed_cases['date'] = pd.to_datetime(self.confirmed_cases[['year', 'month', 'day']])
        self.confirmed_cases = self.confirmed_cases[self.confirmed_cases['date'] >= self.day_data]
        self.incidence_cases = self.confirmed_cases['confirmed_cases_hubei'].values
        self.china_incidence = pd.read_csv(os.path.join(self.data_dir, "china_incidence.csv"))
        self.death = self.china_incidence.mort.values
        self.date_diff = datetime.strptime(self.day_max, "%Y-%m-%d") - datetime.strptime(self.day_data, "%Y-%m-%d")
        self.incidence_deaths_tot = np.concatenate((np.zeros(self.date_diff.days + 1 - len(self.death)), self.death))

        # Correct for confirmed cases in Hubei only (979 total)
        self.incidence_deaths = 979 / sum(self.incidence_deaths_tot) * self.incidence_deaths_tot
        self.incidence_deaths = self.incidence_deaths.round().astype(int)
        print("Incidence deaths", self.incidence_deaths)

        # Age distribution of cases in mainland China as of 2020-02-11
        # (Chinese CDC Weekly, The epidemiological characteristics of an outbreak...)
        self.cases_tmax = np.array([416, 549, 3619, 7600, 8571, 10008, 8583, 3918, 1408])
        self.prop_cases_tmax = self.cases_tmax / sum(self.cases_tmax)

        # Age distribution of deaths in mainland China as of 2020-02-11
        # (Chinese CDC Weekly, The epidemiological characteristics of an outbreak...)
        self.mort_tmax = np.array([0, 1, 7, 18, 38, 130, 309, 312, 208])
        self.prop_mort_tmax = self.mort_tmax / sum(self.mort_tmax)
