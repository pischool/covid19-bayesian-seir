import os
from datetime import timedelta, datetime

import numpy as np
import pandas as pd

from pyexperiment.dataset.dataset import Dataset


class ItalyDataset(Dataset):

    def __init__(self):
        self.data_dir = os.path.join("data", "italy")

        # Controls ----
        self.day_start = "2020-02-07"
        self.day_data = "2020-02-08"
        self.day_max = "2020-03-03"
        self.day_quarantine = "2020-03-08"

        # Age distribution in Italy for 9 age classes
        self.age_dist = self._get_age_structure_from_excel()
        self.age_dist = self.age_dist / sum(self.age_dist)

        # Population in Lombardia + Emilia Romagna + Veneto + Marche + Piemonte
        # (Source Eurostat 2018)
        self.pop_t = int(sum(np.array([10.04e6,4.453e6,4.905e6,1.522e6,4.356e6])))

        # Case incidence in Northern Italy up to 2020-03-12
        # (https://www.epicentro.iss.it/coronavirus/bollettino/Bollettino-sorveglianza-integrata-COVID-19_09-marzo-2020.pdf)
        # (https://github.com/pcm-dpc/COVID-19/blob/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv)
        self.confirmed_cases = df = pd.read_csv(os.path.join(self.data_dir, "italy_data_15march.csv"))
        self.confirmed_cases['year'] = 2020
        self.confirmed_cases['date'] = pd.to_datetime(self.confirmed_cases[['year', 'month', 'day']])
        self.confirmed_cases = self.confirmed_cases[(self.confirmed_cases['date'] >= self.day_data) & (self.confirmed_cases['date'] <= self.day_max)]
        self.incidence_cases = self.confirmed_cases['cases'].astype(int).values

        # Deaths incidence in Northern Italy up to 2020-03-12
        # (https://github.com/pcm-dpc/COVID-19/blob/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv)
        self.incidence_deaths = self.confirmed_cases['deaths'].values
        self.age_distributions_cases_deaths_15march = pd.read_csv(os.path.join(self.data_dir, "age_distributions_cases_deaths_15march.csv"))
        self.cases_tmax = self.age_distributions_cases_deaths_15march['cases'].values
        self.prop_cases_tmax = self.cases_tmax / sum(self.cases_tmax)
        self.mort_tmax = self.age_distributions_cases_deaths_15march['deaths'].values
        self.prop_mort_tmax = self.mort_tmax / sum(self.mort_tmax)
