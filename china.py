import random

from pyexperiment.dataset.china import ChinaDataset
from pyexperiment.model.china import ChinaModel

random.seed(101)


dataset = ChinaDataset()
dataset.show_info()
model = ChinaModel(dataset)
model.run()
model.show_info()
model.save_results()
