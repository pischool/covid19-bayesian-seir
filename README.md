> [*All models are wrong, but some models are useful.*](https://en.wikipedia.org/wiki/All_models_are_wrong) -- Box, G. E. P. (1979), "Robustness in the strategy of scientific model building", in Launer, R. L.; Wilkinson, G. N. (eds.), *Robustness in Statistics*, Academic Press, pp. 201–236.


# Purpose

We think that the machine learning and data science community has untapped
capacity to help with the COVID-19 emergency. Specifically, we can help with
modelling and simulating the propagation of the infection. We want models that can
- learn from observed data
- model interventions
- make projections of future events, inclusive of error bars to represent our uncertainty

Some of the most efficient and useful models nowadays are Bayesian SEIR
(which stands for susceptible-exposed-infected-recovered) propagation models.
There are hardly any such models implemented in a form directly accessible to
most data scientists. For this reason, we have decided to port one existing,
recently published model to Python and release this as open source.

# What we have decided to do

* port [Riou et al 2020 "Adjusted age-specific case fatality ratio during the COVID-19 epidemic in Hubei, China, January and February 2020"](https://github.com/jriou/covid_adjusted_cfr) from R and Stan to Python and pyStan
* gather and create tutorial resources for machine learners and data scientists to get started with modelling
* extend the model: model interventions (non-pharmaceutic interventions, like isolation, shutting schools, etc), allow multi-model prediction etc

# How can you help?
* follow this repo
* read the original [paper](https://github.com/jriou/covid_adjusted_cfr/blob/master/manuscript/manuscript_v2.pdf) and [code](https://github.com/jriou/covid_adjusted_cfr)
* read the tutorial material below
* read the issues list
* pick a topic where help is needed, solve it, and submit a pull request.
* if unsure about pull requests, please check Contributions.md, we have provided a checklist that may help.

### Tutorials

Here are several tutorials to get you started with Bayesian Data Analysis.

- The official Stan documentation - [here](https://mc-stan.org/users/documentation/index.html)
- Introductory paper by Andrew Gelman - [here](http://www.stat.columbia.edu/~gelman/research/published/stan_jebs_2.pdf)
- A workshop by Michael Clark - [here](https://m-clark.github.io/workshops/bayesian/01_intro.html)

### Articles on Statistical Inference for Infectious Diseases
- The Mathematics of Infectious Diseases - [link](http://leonidzhukov.net/hse/2014/socialnetworks/papers/2000SiamRev.pdf)
- Avoidable errors in the modelling of outbreaks of emerging pathogens, with special reference to Ebola - [link](https://royalsocietypublishing.org/doi/pdf/10.1098/rspb.2015.0347)
- Contemporary statistical inference for infectious disease models using Stan - [link](https://arxiv.org/pdf/1903.00423.pdf)
- Estimates of the severity of coronavirus disease 2019: a model-based analysis - [link](https://www.thelancet.com/action/showPdf?pii=S1473-3099%2820%2930243-7)
- Estimating the number of infections and the impact of non-pharmaceutical interventions on COVID-19 in 11 European countries - [link](https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-Europe-estimates-and-NPI-impact-30-03-2020.pdf)


## Installation Instructions
For this project, we need Stiff ODE Solvers, such as 'integrate_ode_bdf'. These functions doesn't ship with PyStan at the moment.
So we have to build from source.

The instructions are posted by Ari Hartikainen [here](https://discourse.mc-stan.org/t/cannot-install-pystan-with-support-for-stiff-ode-sovers/12736/3).

Please follow the below instructions to install PyStan with Stiff ODE Solvers:

1. `git config --system core.longpaths true`

2. `git clone --recursive https://github.com/stan-dev/pystan`

3. `cd pystan`

4. `git checkout cvodes`

5. Remove docs and tests for stan-math libraries

* Using shell script:

```
FOR /D /R "pystan/stan/lib/stan_math/lib" %D IN (doc) DO @IF EXIST "%D" RD /S /Q "%D"
FOR /D /R "pystan/stan/lib/stan_math/lib" %D IN (test) DO @IF EXIST "%D" RD /S /Q "%D"
```

* Using Linux CLI:

```bash
sudo find pystan/stan/lib/stan_math/lib/ -type d -name doc -prune -exec rm -rf {} \;
sudo find pystan/stan/lib/stan_math/lib/ -type d -name test -prune -exec rm -rf {} \;
```

6. `python -m pip install .`


# Run Python Experiment

```bash
virtualvenv -p python3.6 env3.6
source env3.6/bin/activate
pip install -r requirements.txt
python china.py
python italy.py
```

Note: to install Stan using ODE Solvers, please check above section.


# Run R Experiment

Please, check `RGuide.md` file
