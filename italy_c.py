import random

from pyexperiment.dataset.italy import ItalyDataset
from pyexperiment.model.italy_c import ItalyCModel

random.seed(101)


dataset = ItalyDataset()
dataset.show_info()
model = ItalyCModel(dataset)
model.run()
model.show_info()
model.save_results()
